const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

//1.2
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController	=> res.send(
		resultFromController));
});

//1.3
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(
		resultFromController));
});

//4.1
router.post("/checkout", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    const userIsAdmin = auth.decode(req.headers.authorization).isAdmin;
    const productData = req.body;
    userController.addProductToUser(userId, userIsAdmin, productData).then((resultFromController) => res.send(
    	resultFromController))
});

//4.2
router.get("/:userId/userDetails", (req, res) => {
    userController.getUserDetails(req.params).then(resultFromController => res.send(resultFromController));
});


//SG-1
router.put("/:userId/setAsAdmin", (req, res) => {
	userController.setAsAdmin(req.params).then(resultFromController => res.send(
		resultFromController));
});

//SG-2
router.get("/getUserOrders",auth.verify, (req, res) => {
	userController.getUserOrders(req.headers.authorization).then(resultFromController => res.send(
		resultFromController));
});


module.exports = router;